(** {2 Data loading} *)

let header = ["level"; "hash"; "timestamp"; "fitness"; "gas"]

let rows =
  Containers.IO.with_in Sys.argv.(1) @@ fun ic ->
  let csv = Csv.of_channel ~separator:' ' ~has_header:true ic in
  (* Correct the header of the CSV *)
  Csv.Rows.set_header ~replace:true csv header ;
  List.to_seq @@ Csv.Rows.input_all csv

(** {2 Helpers} *)

let get row field = Csv.Row.find row field

let geti row field =
  let s = get row field in
  try int_of_string s
  with Failure _ ->
    Format.eprintf "can't parse '%s' as float@." s ;
    0

let getf row field =
  let s = get row field in
  try float_of_string s
  with Failure _ ->
    Format.eprintf "can't parse '%s' as float@." s ;
    0.0

let head seq =
  match Seq.uncons seq with None -> assert false | Some (hd, _) -> hd

let rec fold_unfold f state seq =
  match seq () with
  | Seq.Nil -> Seq.empty
  | Seq.Cons (elt, tail) ->
      let (state, elt) = f state elt in
      fun () -> Seq.Cons (elt, fold_unfold f state tail)

module Windowed_fold = struct
  module Queue = CCFQueue
  (* This module implements sequence transformers implemented
     by folding over the input sequence a function taking
     a sliding window as argument. *)

  type ('x, 'a) state = { samples : 'x Queue.t; acc : 'a }

  let make_transducer f state next_sample =
    let (produced, acc) = f state.samples next_sample state.acc in
    let (samples, _old_sample) = Queue.take_back_exn state.samples in
    let samples = Queue.cons next_sample samples in
    ({ samples; acc }, produced)

  let apply f initial seq =
    let transducer = make_transducer f in
    fold_unfold transducer initial seq

  let convolute zero add smul kernel seq =
    let nsamples = List.length kernel in
    let initial =
      { samples = Queue.of_list (List.init nsamples (fun _ -> zero)); acc = () }
    in
    let conv samples =
      let samples = Queue.to_list samples in
      List.fold_left add zero (List.map2 smul samples kernel)
    in
    apply (fun samples _next_sample () -> (conv samples, ())) initial seq

  let convolute_float = convolute 0.0 ( +. ) ( *. )
end

let complete seq default initial =
  let rec loop seq index =
    match Seq.uncons seq with
    | None -> Seq.empty
    | Some (((i, _data) as head), tl) ->
        if i < index then invalid_arg "complete"
        else if index = i then fun () -> Seq.Cons (head, loop tl (index + 1))
        else fun () -> Seq.Cons ((index, default), loop seq (index + 1))
  in
  loop seq initial

let () =
  assert (
    List.of_seq (complete (List.to_seq [(0, "a"); (2, "b")]) "default" (-1))
    = [(-1, "default"); (0, "a"); (1, "default"); (2, "b")])

(** {2 Data processing} *)

let date_to_seconds date_string =
  match Ptime.of_rfc3339 date_string with
  | Error _ -> assert false
  | Ok (t, _, _) -> Ptime.to_float_s t

let series =
  Seq.map
    (fun row ->
      let round = getf row "fitness" in
      let gas = getf row "gas" in
      let time = geti row "level" in
      (time, (round, gas)))
    rows

let series =
  let (t0, _) = head series in
  complete series (0.0, 0.0) t0
  |> Seq.map (fun (t, data) -> (float_of_int t, data))

let shifted =
  let (t0, _) = head series in
  Seq.map (fun (t, data) -> ((t -. t0) /. 4096., data)) series

let series_max s = Seq.fold_left Float.max 0.0 s

let max_gas = series_max (Seq.map (fun (_, (_, g)) -> g) series)

let max_round = series_max (Seq.map (fun (_, (r, _)) -> r) series)

let gas_vs_round = Seq.map (fun (_t, (r, g)) -> (g /. max_gas, r)) shifted

let rounds = Seq.map (fun (t, (r, _)) -> (t, r)) shifted

let gas = Seq.map (fun (t, (_, g)) -> (t, g)) shifted

let averaged_rounds =
  let width = 256 in
  let kernel = List.init width (fun _ -> 1. /. float_of_int width) in
  Windowed_fold.convolute_float kernel (Seq.map snd rounds)

let averaged_gas =
  let width = 256 in
  let kernel = List.init width (fun _ -> 1. /. float_of_int width) in
  Windowed_fold.convolute_float kernel (Seq.map snd gas)

let normalize series =
  let max = series_max series in
  Seq.map (fun x -> x /. max) series

let normalized_gas = normalize averaged_gas

let normalized_rounds = normalize averaged_rounds

let () =
  let open Plot in
  run ~target:(qt ~pixel_size:(1000, 980) ()) exec_detach
  @@ plot2
       ~xaxis:"level"
       ~yaxis:"round"
       [Line.line ~points:(averaged_rounds |> Seq.map r1 |> Data.of_seq) ()]

let () =
  let open Plot in
  run ~target:(qt ~pixel_size:(1000, 980) ()) exec_detach
  @@ plot2
       ~xaxis:"level"
       ~yaxis:"gas"
       [Line.line ~points:(normalized_gas |> Seq.map r1 |> Data.of_seq) ()]

(** {2 Trying to infer a linear relationship between gas and rounds} *)

open Dagger.Lmh_inference

let round_and_gas = Array.of_seq (Seq.zip averaged_rounds normalized_gas)

let model =
  let log_score a round gas =
    let predicted = a *. gas in
    Dagger.Log_space.unsafe_cast
    @@ Stats.Pdfs.gaussian_ln ~mean:round ~std:1.0 predicted
  in
  map_log_score (sample (Stats_dist.float 1.0)) @@ fun a ->
  Array.fold_left
    (fun acc (round, gas) -> Dagger.Log_space.mul acc (log_score a round gas))
    Dagger.Log_space.one
    round_and_gas

let rng_state = Dagger.RNG.make_self_init ()

let samples = stream_samples model rng_state

let nsamples = 1000

let samples =
  samples |> Seq.drop 1000 (* burn-in *) |> Seq.take nsamples |> List.of_seq

let () =
  let open Plot in
  run
    ~target:(png ~png_file:"scaling_posterior.png" ())
    exec
    (plot2
       ~xaxis:"a"
       ~yaxis:"freq"
       ~title:"Posterior on scaling parameter"
       [ Histogram.hist
           ~points:(Data.of_list (List.rev_map r1 samples))
           ~bins:50
           () ])

let inferred_scaling =
  (* take the empirical mean *)
  List.fold_left ( +. ) 0.0 samples /. float nsamples

let () = Format.printf "round(t) = %f x normalized_gas(t)@." inferred_scaling

let () =
  let open Plot in
  run ~target:(qt ~pixel_size:(1000, 980) ()) exec_detach
  @@ plot2
       ~xaxis:"level"
       ~yaxis:"round"
       [ Line.line
           ~points:(averaged_rounds |> Seq.map r1 |> Data.of_seq)
           ~legend:"real"
           ();
         Line.line
           ~points:
             (Seq.map (fun g -> inferred_scaling *. g) normalized_gas
             |> Seq.map r1 |> Data.of_seq)
           ~legend:"predicted"
           () ]
